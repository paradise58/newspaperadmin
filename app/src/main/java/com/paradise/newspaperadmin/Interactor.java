package com.paradise.newspaperadmin;

import android.arch.lifecycle.MutableLiveData;
import android.util.Log;
import com.paradise.newspaperadmin.data.StubFb;
import com.paradise.newspaperadmin.data.entity.NewsPart;
import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.util.List;

public class Interactor {
  public final StubFb stubFb;
  private CompositeDisposable compositeDisposable = new CompositeDisposable();
  private MutableLiveData<List<NewsPart>> newsLiveData = new MutableLiveData<>();
  private Flowable<List<NewsPart>> newsPart;

  public Interactor(StubFb stubFb) {
    this.stubFb = stubFb;
  }

  public MutableLiveData<List<NewsPart>> getNewsPartLiveData() {
    newsPart = stubFb.getNewsPartDao().getAll();
    compositeDisposable.add(newsPart.subscribeOn(Schedulers.io())
        .subscribe(result -> newsLiveData.postValue(result),
            error -> Log.e("tag_", error.getLocalizedMessage())));
    return newsLiveData;
  }


}
