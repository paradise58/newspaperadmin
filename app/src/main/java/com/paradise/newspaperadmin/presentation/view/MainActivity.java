package com.paradise.newspaperadmin.presentation.view;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.paradise.newspaperadmin.R;
import com.paradise.newspaperadmin.common.App;
import com.paradise.newspaperadmin.data.StubFb;
import com.paradise.newspaperadmin.data.entity.NewsPart;
import com.paradise.newspaperadmin.presentation.LayoutHelper;
import com.paradise.newspaperadmin.presentation.viewmodel.GlobalViewModelFactory;
import com.paradise.newspaperadmin.presentation.viewmodel.MainViewModel;
import java.util.List;
import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

  @Inject StubFb stubFb;
  @Inject GlobalViewModelFactory factory;
  @Inject LayoutHelper layoutHelper;

  EditText editText;
  FloatingActionButton fab;
  MainViewModel viewModel;
  LinearLayout linearLayout;
  LinearLayout previewContainer;

  View.OnClickListener fabChooseType;
  View.OnClickListener fabAddToPreview;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    App.getAppComponent(this).inject(this);
    viewModel = ViewModelProviders.of(this, factory).get(MainViewModel.class);
    viewModel.getNewsPartLiveData().observe(this, this::addToPreview);
    setContentView(R.layout.activity_main);
    init();
    changeFabListener();
  }

  private void addToPreview(List<NewsPart> newsParts) {
    previewContainer.removeAllViewsInLayout();
    for (NewsPart part : newsParts) {
      addPart(part);
    }
  }

  public void changeFabListener() {
    if (linearLayout.getChildCount() == 0) {
      fab.setOnClickListener(fabChooseType);
      fab.setImageResource(android.R.drawable.ic_menu_add);
    } else {
      fab.setImageResource(android.R.drawable.ic_menu_view);
      fab.setOnClickListener(fabAddToPreview);
    }
  }

  private void init() {
    linearLayout = findViewById(R.id.container);
    previewContainer = findViewById(R.id.preview_container);
    fab = findViewById(R.id.fab);
    fabChooseType = this::showPopUpMenu;
    fabAddToPreview = v -> new AlertDialog.Builder(this).setTitle("Добавить в предосмотр?")
        .setPositiveButton("Да", (dialog, i) -> addToDataBase())
        .setNeutralButton("Нет", null)
        .create()
        .show();
  }

  public void addToDataBase() {
    stubFb.insertNewsPartTitle(editText.getText().toString());
    linearLayout.removeAllViewsInLayout();
    changeFabListener();
  }

  public void showPopUpMenu(final View view) {
    PopupMenu popupMenu = new PopupMenu(this, view);
    popupMenu.inflate(R.menu.menu_main);
    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
      @Override public boolean onMenuItemClick(MenuItem menuItem) {
        int id = menuItem.getItemId();
        switch (id) {
          case R.id.title:
            choosePartToAdd("Введите заголовок");
            return true;
          //case R.id.text:
          //  return true;
          //case R.id.comment:
          //  return true;
          //case R.id.image:
          //  return true;
          default:
            return false;
        }
      }
    });
    popupMenu.show();
  }

  public void choosePartToAdd(String str) {
    editText = new EditText(getApplicationContext());
    editText.setHint(str);
    linearLayout.addView(editText);
    changeFabListener();
  }

  private void addPart(NewsPart part) {
    switch (part.getType()) {
      case NewsPart.TYPE_TITLE:
        addTitle(part.getPayload());
        return;
      case NewsPart.TYPE_TEXT:
        //addText();
        return;
      case NewsPart.TYPE_IMAGE:
        //addImage();
        return;
      case NewsPart.TYPE_COMMENT:
        //addComment();
    }
  }

  public void addTitle(String payLoad) {
    TextView title = layoutHelper.getTitleView(payLoad);
    previewContainer.addView(title);
    title.setOnClickListener(v -> new AlertDialog.Builder(this).setTitle("Удалить этот элемент ? < " + payLoad.substring(0, payLoad.length() > 30 ? 30 : payLoad.length()) + "... >")
        .setPositiveButton("Да", (dialog, i) -> stubFb.deleteByPayLoad(payLoad))
        .setNeutralButton("Нет", null)
        .create()
        .show());
  }

}
