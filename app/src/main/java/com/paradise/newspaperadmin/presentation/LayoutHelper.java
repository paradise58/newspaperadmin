package com.paradise.newspaperadmin.presentation;

import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.paradise.newspaperadmin.common.App;
import com.paradise.newspaperadmin.data.entity.NewsPart;

public class LayoutHelper {


  public TextView getTitleView(String str) {
    TextView view = new TextView(App.getInstance().getApplicationContext());
    view.setTextSize(20);
    Typeface typeface = null;
    view.setTypeface(typeface, Typeface.BOLD);
    view.setPaddingRelative(32, 24, 32, 24);
    view.setTextIsSelectable(true);
    view.setText(str);
    view.setGravity(Gravity.CENTER);
    view.setTextColor(Color.parseColor("#000000"));
    return view;
  }

}
