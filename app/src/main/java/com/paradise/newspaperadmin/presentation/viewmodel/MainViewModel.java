package com.paradise.newspaperadmin.presentation.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import com.paradise.newspaperadmin.Interactor;
import com.paradise.newspaperadmin.data.entity.NewsPart;
import java.util.List;
import javax.inject.Inject;

public class MainViewModel extends ViewModel {

  private final Interactor interactor;

  @Inject public MainViewModel(Interactor interactor) {
    this.interactor = interactor;
  }

  public MutableLiveData<List<NewsPart>> getNewsPartLiveData() {
    return interactor.getNewsPartLiveData();
  }
}
