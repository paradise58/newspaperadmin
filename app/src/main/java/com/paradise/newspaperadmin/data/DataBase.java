package com.paradise.newspaperadmin.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import com.paradise.newspaperadmin.data.dao.NewsPartDao;
import com.paradise.newspaperadmin.data.entity.NewsPart;

@Database(entities = {NewsPart.class}, version = 1, exportSchema = false)
public abstract class DataBase extends RoomDatabase {
  public abstract NewsPartDao newsPartDao();
}
