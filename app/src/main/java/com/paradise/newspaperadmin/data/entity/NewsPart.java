package com.paradise.newspaperadmin.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class NewsPart {

  public static final int TYPE_TITLE = 234;
  public static final int TYPE_IMAGE = 375;
  public static final int TYPE_TEXT = 751;
  public static final int TYPE_COMMENT = 417;

  @PrimaryKey(autoGenerate = true)
 private long id;
  private String payload;
  private int type;
  //private int order;
  //private String uuid;

  public NewsPart(int type, String payload) {
    this.payload = payload;
    this.type = type;
    //this.order = order;
    //this.uuid = uuid;
  }

  public String getPayload() {
    return payload;
  }

  public int getType() {
    return type;
  }

  //public int getOrder() {
  //  return order;
  //}

  //public String getUuid() {
  //  return uuid;
  //}

  public long getId() {
    return id;
  }

  public void setPayload(String payload) {
    this.payload = payload;
  }

  public void setType(int type) {
    this.type = type;
  }

  //public void setOrder(int order) {
  //  this.order = order;
  //}

  //public void setUuid(String uuid) {
  //  this.uuid = uuid;
  //}

  public void setId(long id) {
    this.id = id;
  }
}
