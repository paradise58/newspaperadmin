package com.paradise.newspaperadmin.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import com.paradise.newspaperadmin.data.entity.NewsPart;
import io.reactivex.Flowable;
import java.util.List;

@Dao
public interface NewsPartDao {
@Query("SELECT * FROM NewsPart")
  Flowable<List<NewsPart>> getAll();

@Insert
  void insert(NewsPart newsPart);

@Query("DELETE from NewsPart WHERE payload IN (:item)")
  void delete(String item);

}
