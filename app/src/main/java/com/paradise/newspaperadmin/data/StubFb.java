package com.paradise.newspaperadmin.data;

import com.paradise.newspaperadmin.data.dao.NewsPartDao;
import com.paradise.newspaperadmin.data.entity.NewsPart;
import javax.inject.Inject;

public class StubFb {

  private final NewsPartDao newsPartDao;

  @Inject public StubFb(NewsPartDao newsPartDao) {
    this.newsPartDao = newsPartDao;
  }

  public void insertNewsPartTitle(String payload) {
    new Thread(() -> newsPartDao.insert(new NewsPart(NewsPart.TYPE_TITLE, payload))).start();
  }

  public NewsPartDao getNewsPartDao() {
    return newsPartDao;
  }

  public void deleteByPayLoad(String item) {
    new Thread(() -> newsPartDao.delete(item)).start();
  }
}
