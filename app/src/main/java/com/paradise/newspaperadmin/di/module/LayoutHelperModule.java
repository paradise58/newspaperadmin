package com.paradise.newspaperadmin.di.module;

import com.paradise.newspaperadmin.presentation.LayoutHelper;
import dagger.Module;
import dagger.Provides;

@Module public class LayoutHelperModule {

  @Provides LayoutHelper layoutHelper() {
    return new LayoutHelper();
  }
}
