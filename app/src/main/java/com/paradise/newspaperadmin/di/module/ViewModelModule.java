package com.paradise.newspaperadmin.di.module;

import android.arch.lifecycle.ViewModel;
import com.paradise.newspaperadmin.di.qualifer.ViewModelKey;
import com.paradise.newspaperadmin.presentation.viewmodel.MainViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

  @Binds
  @IntoMap
  @ViewModelKey(MainViewModel.class)
  abstract ViewModel bindMainViewModel(MainViewModel mainViewModel);


}

