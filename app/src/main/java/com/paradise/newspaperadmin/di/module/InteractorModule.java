package com.paradise.newspaperadmin.di.module;

import com.paradise.newspaperadmin.Interactor;
import com.paradise.newspaperadmin.data.StubFb;
import dagger.Module;
import dagger.Provides;

@Module public class InteractorModule {

  @Provides Interactor interactor(StubFb stubFb) {
    return new Interactor(stubFb);
  }
}
