package com.paradise.newspaperadmin.di.module;

import android.content.Context;
import com.paradise.newspaperadmin.Interactor;
import com.paradise.newspaperadmin.common.App;
import com.paradise.newspaperadmin.presentation.LayoutHelper;
import dagger.Module;
import dagger.Provides;

@Module(includes = {RoomModule.class,ViewModelModule.class,InteractorModule.class,LayoutHelperModule.class})

public class AppModule {

  private final App app;

  public AppModule(App app) {
    this.app = app;
  }
  @Provides
  Context provideContext(){
    return app;
  }
  @Provides
  App provideApp(){
    return app;
  }
}
