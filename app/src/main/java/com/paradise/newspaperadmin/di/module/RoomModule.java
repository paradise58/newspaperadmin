package com.paradise.newspaperadmin.di.module;

import android.arch.persistence.room.Room;
import com.paradise.newspaperadmin.common.App;
import com.paradise.newspaperadmin.data.DataBase;
import com.paradise.newspaperadmin.di.scope.AppScope;
import com.paradise.newspaperadmin.data.StubFb;
import com.paradise.newspaperadmin.data.dao.NewsPartDao;
import dagger.Module;
import dagger.Provides;

@Module public class RoomModule {

  private String db_name = "DataBase";

  @Provides @AppScope StubFb stubFb(NewsPartDao newsPartDao) {
    return new StubFb(newsPartDao);
  }

  @Provides NewsPartDao newsPartDao(App app){
    return Room.databaseBuilder(app,DataBase.class,db_name).build().newsPartDao();
  }
}
