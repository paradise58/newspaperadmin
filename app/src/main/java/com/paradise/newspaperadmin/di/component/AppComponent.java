package com.paradise.newspaperadmin.di.component;

import com.paradise.newspaperadmin.Interactor;
import com.paradise.newspaperadmin.di.scope.AppScope;
import com.paradise.newspaperadmin.di.module.AppModule;
import com.paradise.newspaperadmin.presentation.view.MainActivity;
import dagger.Component;

@AppScope
@Component(modules = {AppModule.class})
public interface AppComponent {
  void inject (MainActivity mainActivity);
}
