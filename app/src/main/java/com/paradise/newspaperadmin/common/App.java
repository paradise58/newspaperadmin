package com.paradise.newspaperadmin.common;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDexApplication;
import com.facebook.stetho.Stetho;
import com.paradise.newspaperadmin.di.component.AppComponent;
import com.paradise.newspaperadmin.di.component.DaggerAppComponent;
import com.paradise.newspaperadmin.di.module.AppModule;

public class App extends MultiDexApplication {
protected static App instance;
private  AppComponent component = DaggerAppComponent.builder().appModule(new AppModule(this)).build();

  @Override public void onCreate() {
    super.onCreate();
    instance = this;
    Stetho.initializeWithDefaults(this);
  }

  public static App getInstance() {
    return instance;
  }

  public static AppComponent getAppComponent(Context context) {
    return ((App)context.getApplicationContext()).component;
  }
}
